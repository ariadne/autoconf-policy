ARCH := x86_64
PREFIX := /usr
SHAREDIR := ${PREFIX}/share
BASEDIR := ${SHAREDIR}/autoconf-policy

.PHONY: install

install:
	for i in arch/*; do \
		install -D -m644 $$i ${DESTDIR}${BASEDIR}/$$i; \
	done
	for i in overrides/*; do \
		install -D -m644 $$i ${DESTDIR}${BASEDIR}/$$i; \
	done
	ln -sf ${BASEDIR}/arch/${ARCH} ${DESTDIR}${SHAREDIR}/config.site

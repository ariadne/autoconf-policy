# autoconf-policy

`autoconf-policy` provides helpers that inform autoconf scripts about a given
alpine environment, using the autoconf site config feature.  This feature
works by evaluating the `$CONFIG_SITE` environment variable which defaults to
`/usr/share/config.site`.

We install a symlink at `/usr/share/config.site` which reflects the 'default'
architecture configuration for a host.  This symlink points to a path like
`/usr/share/autoconf-policy/arch/$CARCH`.  In a cross-compile scenario, the
`$CONFIG_SITE` environment variable is updated to point directly at the
arch-specific loader.

The arch-specific loader loads the appropriate autoconf policy snippets for
the specific architecture.

For more information on how this works from the autotools side, look at the
[config.site autoconf manual section](https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.69/html_node/Site-Defaults.html#Site-Defaults),
or the [config.site automake manual section](https://www.gnu.org/software/automake/manual/html_node/config_002esite.html).

## Why?

Two main reasons: it helps improve cross-compilation, by bypassing tests known
to be faulty under cross-compilation.  Also, it allows us to bypass faulty
tests in the general sense, such as autoconf poking at GNU libintl internals
unnecessarily.

The other reason is that a non-trivial amount of builder time on slower archs
is spent evaluating the same autoconf tests over and over again.  By providing
a known source of truth for these tests, we can bypass them.

## Credits

Many of the overrides were pulled from Void and Sabotage Linux initially, but
cleaned up and simplified as needed.

## Installation

```
# make install ARCH=apk_arch_name DESTDIR=whatever
```

## Usage

You can test that everything is properly installed by running a configure
script either with CONFIG_SITE defined in the environment, or with `--prefix=/usr`.

If everything is configured correctly, you will see the following line:

```
localhost:~/pkgconf$ ./configure --prefix=/usr
configure: loading site script /usr/share/config.site
```
